import RPi.GPIO as GPIO                       #Import GPIO library
from picamera import PiCamera
from time import sleep
from time import time
from picamera import PiCamera
from time import sleep

camera = PiCamera()
camera.rotation = 180

GPIO.setmode(GPIO.BOARD)                      #Set GPIO pin numbering

pir = 8                                      #Associate pin 26 to pir

GPIO.setup(pir, GPIO.IN)                      #Set pin as GPIO in 

print ("Waiting for sensor to settle")

sleep(2)                   #Waiting 2 seconds for the sensor to initiate

print ("Detecting motion")

recording=False
vid = 0

while True:

    while GPIO.input(pir):             #Check whether pir is HIGH
        #if recording == False:
            vid += 1
            recording = True
            print('recording ', vid)
            #camera.start_recording('/home/pi/Videos/video_'+str(vid)+'.h264')
            camera.capture('/home/pi/Pictures/pic_'+str(vid)+'.jpg')
            start=time()
            
    #sensor detects for a while (depends on knob), then waits a while and detects again  
    #sleep(4.5)	 		#4 seems to be the longest delay actually needed to detect again
    if recording == True:
        while (time()-start < 4.5):
            vid += 1
            print('still recording ', vid)
            #camera.start_recording('/home/pi/Videos/video_'+str(vid)+'.h264')
            camera.capture('/home/pi/Pictures/pic_'+str(vid)+'.jpg')
    
    #check if motion has actually stopped
    if GPIO.input(pir) == 0:
        if recording == True:
            #camera.stop_recording()
            print('recording stoped ', time()-start)
            recording = False

    sleep(0.1)  #While loop delay should be less than detection delay

