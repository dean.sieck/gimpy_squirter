import sys
sys.path.append("/home/pi/Gimpy")

import time
import numpy as np
import cv2 as cv
import RPi.GPIO as GPIO                       #Import GPIO library
from picamera2 import Picamera2, Preview
from functions.Pan_Tilt_Control import pan_tilt

pt=pan_tilt([100,110])

#initialize camera
camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
camera.configure(camera_config)
#camera.start_preview(Preview.QTGL)
camera.start()
img = camera.capture_array()

ball_d=39.6
dist=785

minDist = 100
param1 = 30 #500
param2 = 50 #200 #smaller value-> more false circles
minRadius = 5
maxRadius = 200 #10

path='/home/pi/Gimpy/test_pics/'
finished=False
while not finished:
    time.sleep(1)
    img = camera.capture_array()
    red=img[:,:,0]
    blurred = cv.medianBlur(red, 25) #cv.bilateralFilter(gray,10,50,50)

    # docstring of HoughCircles: HoughCircles(image, method, dp, minDist[, circles[, param1[, param2[, minRadius[, maxRadius]]]]]) -> circles
    circles = cv.HoughCircles(blurred, cv.HOUGH_GRADIENT, 1, minDist, param1=param1, param2=param2, minRadius=minRadius, maxRadius=maxRadius)
    if circles is not None:
        circles = np.uint16(np.around(circles))
        for i in circles[0,:]:
            cv.circle(img, (i[0], i[1]), i[2], (0, 255, 0), 2)
            cv.imshow('img', cv.resize(img,(960, 540)))
            cv.waitKey(1)
    else:
        print('No circles found')
    

    #print(circles)
    pic_d=circles[0,0,2]*2
    pic_y=circles[0,0,1]
    f=pic_d*dist/ball_d
    #print('f='+str(f))
    
    finished,_=pt.approach_cord(circles[0,0,:2],threshold=10)
        
# Show result for testing:
cv.imshow('img', cv.resize(img,(960, 540)))
cv.waitKey(0)
cv.destroyAllWindows()
 

