import cv2
import csv
import numpy as np

location = (300,200)

file = open('nogo.txt','r')
reader = csv.reader(file,quoting = csv.QUOTE_NONNUMERIC)

polygons=[]
points=np.zeros((0,2))

for row in reader:
    if row == []:
        #finish last region
        polygons.append(points)
        #create a new point array for new region
        points=np.zeros((0,2))
    else:
        # add point to current point array
        points=np.append(points,[row],axis=0).astype(int)
        
polygons.append(points)

        
#check if point is in polygon
closest = -10000
for contour in polygons:
    # positive number means inside contour, negative number means outside
    distance = cv2.pointPolygonTest(contour, location, True) #third argument finds shortest distance to contour
    if distance>closest:
        closest = distance
    if distance>=0:
        print('inside zone:', distance)
    else:
        print('outside zone:', distance)
print (closest)
