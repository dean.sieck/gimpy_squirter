import cv2 as cv
import numpy as np
import csv

def mousePoints(event,x,y,flags,params):
    global counter, clicked, point_matrix, finished
    
    # Left button mouse click event opencv
    if event == cv.EVENT_LBUTTONDOWN:
        point_matrix= np.append(point_matrix,np.zeros((1,2),int),axis=0)
        point_matrix[counter] = x,y
        counter = counter + 1
        clicked = True
    elif event == cv.EVENT_MBUTTONDOWN:
        clicked = True
        finished = True
        print('Finished')


def poly(orig_img):
    img = orig_img.copy()
    global counter, clicked, point_matrix, finished
    point_matrix = np.zeros((0,2),int)
    counter = 0
    clicked = False
    finished = False
    print('left click to add points')
    print('middle click to finish')

    while finished == False:
        
        # Showing original image
        cv.imshow("Original Image ", img)
        cv.waitKey(1)
        while clicked == False:
            # Mouse click event on original image
            cv.setMouseCallback("Original Image ", mousePoints)
            # Refreshing window all time
            cv.waitKey(1)
        #print(point_matrix)
        clicked = False
     
        if counter>1:
            #show the zone you are drawing      
            cv.fillPoly(img, [point_matrix], 255)
            
        if finished==True:
            return point_matrix
        

def rect(orig_img):
    img = orig_img.copy()
    global counter, clicked, point_matrix, finished
    point_matrix = np.zeros((0,2),int)
    counter = 0
    clicked = False
    finished = False
    print('left click to add points')

    while finished == False:
        
        # Showing original image
        cv.imshow("Original Image ", img)
        cv.waitKey(1)
        while clicked == False:
            # Mouse click event on original image
            cv.setMouseCallback("Original Image ", mousePoints)
            # Refreshing window all time
            cv.waitKey(1)
        #print(point_matrix)
        clicked = False
     
        if counter>1:
            #show the zone you are drawing      
            cv.fillPoly(img, [point_matrix], 255)
            
        if counter>=4:
            finished==True
            cv.imshow("Original Image ", img)
            cv.waitKey(1)
            return point_matrix