import cv2
import numpy as np
import csv

image_path = '/home/pi/Gimpy/test_pics/90_60.jpg'
image_size = (700, 950)
 
# Create point matrix get coordinates of mouse click on image
point_matrix = np.zeros((0,2),int)

#save regions to a file
file = open('/home/pi/Gimpy/Constants/nogo.txt','w')
writer = csv.writer(file)
 
counter = 0
clicked = False
finished = False
def mousePoints(event,x,y,flags,params):
    global counter, clicked, point_matrix, finished
    
    # Left button mouse click event opencv
    if event == cv2.EVENT_LBUTTONDOWN:
        point_matrix= np.append(point_matrix,np.zeros((1,2),int),axis=0)
        point_matrix[counter] = x,y
        counter = counter + 1
        clicked = True
    elif event == cv2.EVENT_MBUTTONDOWN:
        clicked = True
        finished = True
        print('Finished')
 
# Read image
img = cv2.imread(image_path)
img= cv2.resize(img, image_size)
img_copy = img.copy()
 
while finished == False:
    
    # Showing original image
    cv2.imshow("Original Image ", img)
    cv2.waitKey(1)
    while clicked == False:
        # Mouse click event on original image
        cv2.setMouseCallback("Original Image ", mousePoints)
        # Refreshing window all time
        cv2.waitKey(1)
    print(point_matrix)
    clicked = False

 
    if counter>1:
        #show the zone you are drawing      
        cv2.fillPoly(img, [point_matrix], 255)
        
    if finished==True:
        x = input('save zone? (y/n) ')
        if x == 'y':
            writer.writerows(point_matrix)
        x = input('Create new zone? (y/n) ')
        if x == 'y':
            finished = False
            writer.writerow('')
            counter = 0
            point_matrix = np.zeros((0,2),int)
        else:
            finished = True
            
cv2.waitKey(0)
#save zone to file



#close everything
file.close()
cv2.destroyAllWindows()