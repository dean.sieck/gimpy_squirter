import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import functions.draw_zone as draw
import functions.pt_kinematics as kine

#load camera parameters
mtx = np.loadtxt("/home/pi/Gimpy/Constants/Camera/mtx.csv", delimiter=',')
dist = np.loadtxt("/home/pi/Gimpy/Constants/Camera/dist.csv", delimiter=',')

# Load the image
img = cv.imread('/home/pi/45_0.jpg')
#undistort the image
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
img = cv.undistort(img, mtx, dist, None, newcameramtx)
#Resize so it fits on the screen
scale=0.5
img = cv.resize(img, (0, 0), fx = scale, fy = scale)
 
cv.imshow("Original Image ", img)
cv.waitKey(1)


# All points are in format [cols, rows]
input_pts = draw.rect(img).astype('float32')
#adjust for scale
input_pts=input_pts*1/scale
#input_pts-mtx[(0,1),2]

side1 = float(input ('length of side 1 (cm): '))*100
side2 = float(input ('length of side 2 (cm): '))*100

output_pts = np.float32([[0, 0],
                        [0, side1],
                        [side2, side1],
                        [side2, 0]])

# Compute the perspective transform M
# zone_cord = M * pic_cord
M = cv.getPerspectiveTransform(input_pts,output_pts)
z_M_p= np.matrix(M)
p_M_z= np.linalg.inv(M)

#Goal: zone_picture_camera_world
    #can then do world_camera2_picture2
    #note picture_camera is just a translation by focal length in z direction


#Current goal: picture1_camera1_camera2_picture2

#focal length of camera
f=np.mean([mtx[0,0],mtx[1,1]])
#transformation matrix between cameras
w_T_c1=kine.transform_matrix([0,45])
w_T_c2=kine.transform_matrix([30,45])
c2_T_c1=np.linalg.inv(w_T_c2)*w_T_c1


#new array for new points
c2_pts=np.zeros([4,2])

#center origin of picture coordinates
p1=input_pts-mtx[(0,1),2]

for i in range (0,4):
    #z= np.matrix([output_pts[i,0],output_pts[i,1],1]).transpose()
    #z2=z_M_p*p
    #z2=z2*(1/z2[2]) #normalized

    #camera frame coordiantes    
    c1= np.matrix([p1[i,0],p1[i,1],f,1]).transpose()
    c2=c2_T_c1*c1
    print(c2)
    #adjust back to picture coordinates
    p2=c2[:2].transpose()#+mtx[(0,1),2]
    p2=p2*0.5 #dont do this for real nogo zone
    print(p2)
    #save in array to draw zone
    c2_pts[i,:]=p2
    

# Load the second image
img = cv.imread('/home/pi/45_30.jpg')
c2_pts=np.minimum(c2_pts,[h,w]).astype(int)
#undistort the image
img = cv.undistort(img, mtx, dist, None, newcameramtx)
#Resize so it fits on the screen
img = cv.resize(img, (0, 0), fx = scale, fy = scale)
print(c2_pts)
cv.fillPoly(img, [c2_pts], 255)
cv.imshow("Second Image ", img)


cv.waitKey(0)
cv.destroyAllWindows()