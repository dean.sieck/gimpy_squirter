import numpy as np
import cv2 as cv
from matplotlib import pyplot as plt

# Camera Calibration Matrix (Calculated, do not change)
K=np.asarray([[2.09283247e+03, 0.00000000e+00, 9.23697434e+02],
 [0.00000000e+00, 2.07836329e+03, 6.17731015e+02],
 [0.00000000e+00, 0.00000000e+00, 1.00000000e+00]])

#Load Images
img1 = cv.imread('/home/pi/image1.jpg', cv.IMREAD_GRAYSCALE)  #queryimage # left image
img2 = cv.imread('/home/pi/image2.jpg', cv.IMREAD_GRAYSCALE) #trainimage # right image

#Find matches between images with SIFT
sift = cv.SIFT_create()
# find the keypoints and descriptors with SIFT
kp1, des1 = sift.detectAndCompute(img1,None)
kp2, des2 = sift.detectAndCompute(img2,None)
# FLANN parameters
FLANN_INDEX_KDTREE = 1
index_params = dict(algorithm = FLANN_INDEX_KDTREE, trees = 5)
search_params = dict(checks=50)
flann = cv.FlannBasedMatcher(index_params,search_params)
matches = flann.knnMatch(des1,des2,k=2)
pts1 = []
pts2 = []
# ratio test as per Lowe's paper
for i,(m,n) in enumerate(matches):
    if m.distance < 0.8*n.distance:
        pts2.append(kp2[m.trainIdx].pt)
        pts1.append(kp1[m.queryIdx].pt)
 
 
#Find the Fundamental Matrix
pts1 = np.int32(pts1)
pts2 = np.int32(pts2)
F, mask = cv.findFundamentalMat(pts1,pts2,cv.FM_LMEDS)
# We select only inlier points
pts1 = pts1[mask.ravel()==1]
pts2 = pts2[mask.ravel()==1]
print(F)

#find essential matrix
#E=np.zeros(3,3)
#cv.essentialFromFundamental(F,K,K,E)
