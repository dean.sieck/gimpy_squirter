import cv2 as cv
import numpy as np
import matplotlib.pyplot as plt
import functions.draw_zone as draw
import functions.pt_kinematics as kine

#load camera parameters
mtx = np.loadtxt("/home/pi/Gimpy/Constants/Camera/mtx.csv", delimiter=',')
dist = np.loadtxt("/home/pi/Gimpy/Constants/Camera/dist.csv", delimiter=',')

# Load the image
img = cv.imread('/home/pi/test.jpg')
#undistort the image
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
img = cv.undistort(img, mtx, dist, None, newcameramtx)
#Resize so it fits on the screen
scale=0.5
img = cv.resize(img, (0, 0), fx = scale, fy = scale)
 
cv.imshow("Original Image ", img)
cv.waitKey(1)


# All points are in format [cols, rows]
input_pts = draw.rect(img).astype('float32')
#adjust for scale
input_pts=input_pts*1/scale
#input_pts-mtx[(0,1),2]

side1 = float(input ('length of side 1 (cm): '))*100
side2 = float(input ('length of side 2 (cm): '))*100

output_pts = np.float32([[0, 0, 0],
                        [0, side1, 0],
                        [side2, side1, 0],
                        [side2, 0, 0]])

#get camera frame relative to zone frame
_,rot,P =cv.solvePnP(output_pts,input_pts,mtx,dist)
#convert into translation matrix
c_T_z=np.matrix(np.zeros([4,4]))
R,_=cv.Rodrigues(rot)
c_T_z[0:3,0:3]=R
c_T_z[0:3,3]=P
c_T_z[3,3]=1

#get translation matrix from current joint positions to world
w_T_c=kine.transform_matrix([0,90])

#get zone coordinates in world frame
i=0
z= np.matrix([output_pts[i,0],output_pts[i,1],0,1]).transpose()
world_cord=w_T_c*c_T_z*z
print(np.around(world_cord/100,1))

#are xy directions in picture causing erronious negatives in y direction?


cv.waitKey(0)
cv.destroyAllWindows()