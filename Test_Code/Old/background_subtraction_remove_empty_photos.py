import cv2
import numpy as np
import os
import re 

def sorted_nicely( l ): 
    """ Sort the given iterable in the way that humans expect.""" 
    convert = lambda text: int(text) if text.isdigit() else text 
    alphanum_key = lambda key: [ convert(c) for c in re.split('([0-9]+)', key) ] 
    return sorted(l, key = alphanum_key)


#cap = cv2.VideoCapture(0) # Read video stream from default camera

# Create a background subtractor object using MOG2 algorithm
#fgbg = cv2.createBackgroundSubtractorMOG2()
# create a kernel for opening
#kernel = np.ones((5,5),np.uint8)

directory = 'Pictures/'
sums=[]
good_count=0
bad_count=0

last_frame = np.zeros((1080,1920), np.uint8)

# iterate over files in that directory
for filename in sorted_nicely(os.listdir(directory)):    
    # Read a frame from the video stream
    image = cv2.imread((directory+filename))
    frame = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        
    # Apply background subtraction to the current frame
    #sub_mask = fgbg.apply(frame)
    sub_mask = cv2.subtract(frame,last_frame)
    ret,bw_mask = cv2.threshold(sub_mask,10,255,cv2.THRESH_BINARY)
    fgmask = cv2.morphologyEx(bw_mask, cv2.MORPH_OPEN, kernel)
    diff = sum(sum(fgmask))
    sums.append(diff)
    
    
    if diff>10000:
        # Display the foreground mask
        #cv2.imshow('Foreground Mask', fgmask)
        good_count+=1
        last_frame=frame

    else:
        bad_count+=1
        print(filename, diff)
        os.remove((directory+filename))
#         i1=cv2.resize(image, (960,540))
#         i2=cv2.resize(last_frame, (960,540))
#         cv2.imshow('current', i1)
#         cv2.imshow('last', i2)
#         key = cv2.waitKey(0)
    
    # Exit if 'q' is pressed
    if cv2.waitKey(1) == ord('q'):
        break


print('good count ', good_count)
print('bad count ', bad_count)

print('mean ',sum(sums)/len(sums))
print('max ',max(sums))
print('min ',min(sums))


# Release the video stream and destroy all windows
#cap.release()
cv2.destroyAllWindows()