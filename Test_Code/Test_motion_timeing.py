import RPi.GPIO as GPIO                       #Import GPIO library
from picamera import PiCamera
from time import sleep
from time import time

GPIO.setmode(GPIO.BOARD)                      #Set GPIO pin numbering

pir = 8                                      #Associate pin 26 to pir

GPIO.setup(pir, GPIO.IN)                      #Set pin as GPIO in 

print ("Waiting for sensor to settle")

sleep(2)                   #Waiting 2 seconds for the sensor to initiate

print ("Detecting motion")

first_high=False
first_low=True
start_high=0
start_low=0

while True:

    if GPIO.input(pir):             #Check whether pir is HIGH
        if first_low == True:
            first_high =True
            first_low = False
            start_high = time()
            end_low = time()
            print ("Motion Detected!")
            print('low time: ', start_low-end_low)
        #camera.start_recording('/home/pi/Desktop/video.h264')
        #sleep(5)
        
    if GPIO.input(pir) == 0:
        if first_high == True:
            first_low =True
            first_high = False
            start_low = time()
            end_high = time()
            print ("No Motion")
            print('high time: ', start_high-end_high)
        #camera.stop_recording()

    sleep(0.1)  #While loop delay should be less than detection delay
