import numpy as np
import cv2 as cv
import glob
import os
import functions.pt_kinematics as kine

n=7			#number of grids
s=149.88	#total grid size

# termination criteria
criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
# prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
objp = np.zeros((n*n,3), np.float32)
objp[:,:2] = ((np.mgrid[0:n,0:n])*s/n).T.reshape(-1,2)
# Arrays to store object points and image points from all the images.
objpoints = [] # 3d point in real world space
imgpoints = [] # 2d points in image plane.
images = glob.glob('/home/pi/Gimpy/test_pics/*.jpg')
for fname in images:
    print(fname)
    img = cv.imread(fname)
    gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
    # Find the chess board corners
    ret, corners = cv.findChessboardCorners(gray, (n,n), None)
    # If found, add object points, image points (after refining them)
    if ret == True:
        print('good one')
        objpoints.append(objp)
        corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
        imgpoints.append(corners2)
        # Draw and display the corners
        cv.drawChessboardCorners(img, (6,6), corners2, ret)
        cv.imshow('img', img)
        cv.waitKey(0)
    else:
        print('bad')
        os.remove(fname)
cv.destroyAllWindows()

#Calibration
ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

c1_T_z=np.matrix(np.identity(4))
R,_=cv.Rodrigues(rvecs[0])
c1_T_z[0:3,0:3]=R
c1_T_z[0:3,3]=tvecs[0]
z_T_c1=np.linalg.inv(c1_T_z)

c2_T_z=np.matrix(np.identity(4))
R,_=cv.Rodrigues(rvecs[1])
c2_T_z[0:3,0:3]=R
c2_T_z[0:3,3]=tvecs[1]
z_T_c2=np.linalg.inv(c2_T_z)

c1_T_c2=c1_T_z*z_T_c2
print(np.around(c1_T_c2,2))


w_K_c1=kine.transform_matrix([13,90])
c1_K_w=np.linalg.inv(w_K_c1)
w_K_c2=kine.transform_matrix([35,90])
c2_K_w=np.linalg.inv(w_K_c2)
c1_K_c2=c1_K_w*w_K_c2
print(np.around(c1_K_c2,2))

