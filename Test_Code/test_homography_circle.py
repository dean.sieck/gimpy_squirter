import re
import glob
import numpy as np
import cv2 as cv
import functions.draw_zone as draw
from functions.Pan_Tilt_Control import pan_tilt

pt=pan_tilt()

#ball size
ball_d=39.6
#focal length (f=pic_d*dist/ball_d)
f=2834

#Circle finding parameters
minDist = 100
param1 = 30 #500
param2 = 50 #200 #smaller value-> more false circles
minRadius = 5
maxRadius = 200 #10

#load images
path='/home/pi/Gimpy/Constants/Camera_far_focus/'

# Camera Intrinsic matrix
mtx = np.loadtxt(path+'mtx.csv', delimiter=',')
dist = np.loadtxt(path+'dist.csv', delimiter=',')

images = glob.glob('/home/pi/Gimpy/test_pics/*.jpg')

# get the first picture and find the circle
img = cv.imread(images[0])
#get the joint angles from the image name
joints= [int(x) for x in re.findall(r'\d+', images[0])]
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
#img = cv.undistort(img, mtx, dist, None, newcameramtx)
scale1=1
scale=0.5
img = cv.resize(img, (0, 0), fx = scale1, fy = scale1)
#move this to before resize later -> will need to adjust for display size
red=img[:,:,0]-img[:,:,1]
blurred = cv.medianBlur(red, 25) #cv.bilateralFilter(gray,10,50,50)

#find circles
circles = cv.HoughCircles(blurred, cv.HOUGH_GRADIENT, 1, minDist, param1=param1, param2=param2, minRadius=minRadius, maxRadius=maxRadius)
if circles is not None:
    circles = np.uint16(np.around(circles))
    for circle in circles[0,:]:
        cv.circle(img, (circle[0], circle[1]), circle[2], (0, 255, 0), 2)
cv.imshow('Original img', cv.resize(img,(960, 540)))
cv.waitKey(0)
        
#calculate distance to circle
pic_d=circle[2]*2
dist = f*pic_d/ball_d
x_f=circle[0]/mtx[0,0]
y_f=circle[1]/mtx[1,1]

z=np.sqrt(np.square(dist)/(np.square(x_f)+np.square(y_f)+1))
x=z*x_f
y=z*y_f
cam_cord=np.matrix([x,y,z,1]).transpose()

# # get transformation matrix from camera 1 to world (world = camera1 for this test)
w_T_c1=pt.transform_matrix(joints)
c1_T_w=np.linalg.inv(w_T_c1)

world_cord=w_T_c1*cam_cord


####################################################
#############     RUN TIME CODE        #############
####################################################



#for each image
for fname in images:
# if True:
#     fname=images[0]
    img2 = cv.imread(fname)
    joints= [int(x) for x in re.findall(r'\d+', fname)]
    #calculate transformation from camera2 to world
    w_T_c2=pt.transform_matrix(joints)
    c2_T_w=np.linalg.inv(w_T_c2)


    #transpose circle coordinates for the image
    cam2_cord=np.matrix(np.zeros([4,4]))
    I_=np.matrix(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0]]))

    cam2_cord=c2_T_w*world_cord
    pic2_cord=mtx*I_*cam2_cord
    pic2_cord=pic2_cord*(1/pic2_cord[2])
    xc= int(mtx[0,0]*cam2_cord[0]/cam2_cord[2])
    yc= int(mtx[1,1]*cam2_cord[1]/cam2_cord[2])
    
    #draw circle on image
    cv.circle(img2, (xc, yc), circle[2], (0, 255, 0), 2)

    #undistort the image
    #img2 = cv.undistort(img2, mtx, dist, None, newcameramtx)
    #Resize so it fits on the screen
    #img2 = cv.resize(img2, (0, 0), fx = scale, fy = scale)
    img2_small = cv.resize(img2, (0, 0), fx = scale, fy = scale)
    cv.imshow(str(joints), img2_small)
    cv.waitKey(1)


cv.waitKey(0)
cv.destroyAllWindows()
