import numpy as np
import cv2 as cv
import functions.draw_zone as draw


# rotation and translation from chess boards to camera
rvecs= (np.array([[ 0.10718821],[-0.03355185],[-1.35342043]]), np.array([[0.01623853],[0.01868605],[1.55045078]]), np.array([[ 0.06812693],[-0.40841056],[ 0.09043341]]), np.array([[0.20493132],[0.13964657],[1.46474679]]), np.array([[-0.19545048],[ 0.035742  ],[ 0.05734151]]), np.array([[ 0.00930056],[-0.05582527],[ 1.39554113]]), np.array([[-0.04749   ],[-0.04298988],[ 1.40981863]]), np.array([[-0.0355417 ],[ 0.02198182],[ 0.00368623]]), np.array([[-0.02080894],[-0.05862326],[ 1.41135955]]), np.array([[-0.06151558],[-0.06569042],[ 1.41646826]]), np.array([[-0.03540012],[-0.03809733],[ 1.40922887]]), np.array([[-0.00577022],[ 0.06589567],[ 0.11966288]]), np.array([[-0.07299787],[ 0.09893967],[ 0.10088302]]))
tvecs= (np.array([[-87.22526096],[-18.66512429],[892.80050928]]), np.array([[135.53137769],[ -6.46236071],[644.82577213]]), np.array([[  17.77860812],[-127.94487767],[ 864.05165445]]), np.array([[161.83637372],[-23.74159072],[639.37498986]]), np.array([[-33.76183642],[ 47.079371  ],[923.11349595]]), np.array([[ 196.38876713],[-125.75811612],[ 759.9128154 ]]), np.array([[ -45.62445751],[-124.43341898],[ 543.97371224]]), np.array([[ 61.82967089],[  2.26713917],[878.91381521]]), np.array([[ 125.80714606],[-101.67148862],[ 777.98593227]]), np.array([[-108.38475279],[-122.33989558],[ 551.61770728]]), np.array([[ -36.75090432],[-125.76884896],[ 540.88417452]]), np.array([[151.06410412],[-67.10527097],[894.97168938]]), np.array([[ 79.63041634],[-86.15009763],[660.37003771]]))
#size of real world chess board zone
zone_pts = np.float32([[0, 0],[0, 107],[107, 107],[107, 0]])
zone3d_pts = np.float32([[0, 0,0],[0, 107,0],[107, 107,0],[107, 0,0]])

# Camera Intrinsic matrix
mtx = np.loadtxt("/home/pi/Gimpy/Constants/Camera/mtx.csv", delimiter=',')
dist = np.loadtxt("/home/pi/Gimpy/Constants/Camera/dist.csv", delimiter=',')


# get the first picture and draw a box around the chess board
img = cv.imread('/home/pi/Gimpy/Constants/Camera/Pictures/0.jpg')
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
#img = cv.undistort(img, mtx, dist, None, newcameramtx)
scale=0.5
img = cv.resize(img, (0, 0), fx = scale, fy = scale)
cv.imshow("Original Image ", img)
cv.waitKey(1)
input_pts = draw.rect(img).astype('float32')
input_pts = input_pts*1/scale
#input_pts = input_pts-mtx[0:2,2]

#get camera frame relative to zone frame
_,rot,P =cv.solvePnP(zone3d_pts,input_pts,mtx,dist)
#convert into translation matrix
c1_T_z=np.matrix(np.identity(4))
R,_=cv.Rodrigues(rot)
c1_T_z[0:3,0:3]=R
c1_T_z[0:3,3]=P
z_T_c1=np.linalg.inv(c1_T_z)

# # get transformation matrix from camera 1 to world (world = camera1 for this test)
#w_T_c=kine.transform_matrix([0,90])
w_T_c1=np.matrix(np.identity(4))
c1_T_w=np.linalg.inv(w_T_c1)

world_cord=np.matrix(np.zeros([4,4]))
for i in range (0,4):
    #save world coordinates
    z= np.matrix([zone_pts[i,0],zone_pts[i,1],0,1]).transpose()
    world_cord[:,i]=w_T_c1*c1_T_z*z


####################################################
#############     RUN TIME CODE        #############
####################################################



#for each image
for pic in range(0,1):
    #get transformation matrix from camera2 to zone (only for testing chessboards)
    c2_T_z=np.matrix(np.identity(4))
    R,_=cv.Rodrigues(rvecs[pic])
    c2_T_z[0:3,0:3]=R
    c2_T_z[0:3,3]=tvecs[pic]
    z_T_c2=np.linalg.inv(c2_T_z)

    #calculate transformation from camera2 to world
    #w_T_c=kine.transform_matrix([0,90])
    #c_T_w=np.linalg.inv(w_T_c)
    c2_T_w=c2_T_z*z_T_c1


        #for each corner point
        #get zone coordinates from c1 frame to c2 frame
    output_pts=np.zeros([4,2])
    cam2_cord=np.matrix(np.zeros([4,4]))
    I_=np.matrix(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0]]))
    for i in range (0,4):
        cam2_cord[:,i]=c2_T_w*world_cord[:,i]
        pic2_cord=mtx*I_*cam2_cord[:,i]
        pic2_cord=pic2_cord*(1/pic2_cord[2])
        output_pts[i,:]=pic2_cord[:2].transpose()*scale
#     for i in range (0,4):
#         #use world cord in real time
#         cam2_cord=c2_T_w*world_cord[:,i]
#         pic2_cord=mtx*cam2_cord[0:3]
#         #zone2_cord=np.delete(zone2_cord,2,0)# delete the z dimension
#         #pic2_cord=p_M_z*zone2_cord
#         pic2_cord=pic2_cord*(1/pic2_cord[2]) #normalized
#         output_pts[i,:]=pic2_cord[:2].transpose()
  
 

    # Load the next image
    img2 = cv.imread('/home/pi/Gimpy/Constants/Camera/Pictures/'+str(pic)+'.jpg')
    c2_pts=np.minimum(output_pts,[h,w]).astype(int)
    #undistort the image
    #img2 = cv.undistort(img2, mtx, dist, None, newcameramtx)
    #Resize so it fits on the screen
    img2 = cv.resize(img2, (0, 0), fx = scale, fy = scale)
    cv.fillPoly(img2, [c2_pts], 255)
    cv.imshow(str(pic)+' Image', img2)


cv.waitKey(0)
cv.destroyAllWindows()
