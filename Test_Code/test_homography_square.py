import re
import glob
import numpy as np
import cv2 as cv
import Test_Code.Old.draw_zone as draw
from functions.Pan_Tilt_Control import pan_tilt
pt=pan_tilt()

#size of real world zone
s=149.88
zone_pts = np.float32([[0,0,0],[0,s,0],[s,s,0],[s,0,0]])
#load images
path='/home/pi/Gimpy/Constants/Camera/'
images = glob.glob(path+'Pictures/m_*.jpg')
images = glob.glob('/home/pi/Gimpy/test_pics/*.jpg')

# Camera Intrinsic matrix
mtx = np.loadtxt(path+'mtx.csv', delimiter=',')
dist = np.loadtxt(path+'dist.csv', delimiter=',')


# get the first picture and draw a box around the zone
img = cv.imread(images[0])
img = cv.imread('/home/pi/Gimpy/test_pics/90_67.jpg')

joints= [int(x) for x in re.findall(r'\d+', images[0])]
h,  w = img.shape[:2]
newcameramtx, roi = cv.getOptimalNewCameraMatrix(mtx, dist, (w,h), 1, (w,h))
#img = cv.undistort(img, mtx, dist, None, newcameramtx)
scale1=1
scale=0.5
img = cv.resize(img, (0, 0), fx = scale1, fy = scale1)
cv.imshow("Original Image ", img)
cv.waitKey(1)
input_pts = draw.rect(img).astype('float32')
input_pts = input_pts*1/scale1
#input_pts = input_pts-mtx[0:2,2]

side1 = float(input ('length of side 1 (cm): '))*100
side2 = float(input ('length of side 2 (cm): '))*100
zone_pts = np.float32([[0, 0, 0],
                        [0, side1,0],
                        [side2, side1,0],
                        [side2, 0,0]])

#get camera frame relative to zone frame
_,rot,P =cv.solvePnP(zone_pts,input_pts,mtx,dist)
#convert into translation matrix
c1_T_z=np.matrix(np.identity(4))
R,_=cv.Rodrigues(rot)
c1_T_z[0:3,0:3]=R
c1_T_z[0:3,3]=P
z_T_c1=np.linalg.inv(c1_T_z)
w_T_c1=np.matrix(np.identity(4))

# # get transformation matrix from camera 1 to world (world = camera1 for this test)
w_T_c1=pt.transform_matrix(joints)
c1_T_w=np.linalg.inv(w_T_c1)

world_cord=np.matrix(np.zeros([4,4]))
for i in range (0,4):
    #save world coordinates
    z= np.matrix([zone_pts[i,0],zone_pts[i,1],0,1]).transpose()
    world_cord[:,i]=w_T_c1*c1_T_z*z


####################################################
#############     RUN TIME CODE        #############
####################################################



#for each image
for fname in images:
    img2 = cv.imread(fname)
    joints= [int(x) for x in re.findall(r'\d+', fname)]
    #calculate transformation from camera2 to world
    w_T_c2=pt.transform_matrix(joints)
    c2_T_w=np.linalg.inv(w_T_c2)


        #for each corner point
        #get zone coordinates from c1 frame to c2 frame
    output_pts=np.zeros([4,2])
    cam2_cord=np.matrix(np.zeros([4,4]))
    I_=np.matrix(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0]]))
    for i in range (0,4):
        cam2_cord[:,i]=c2_T_w*world_cord[:,i]
        pic2_cord=mtx*I_*cam2_cord[:,i]
        pic2_cord=pic2_cord*(1/pic2_cord[2])
        output_pts[i,:]=pic2_cord[:2].transpose()*scale

 

    #draw points on image
    c2_pts=output_pts.astype(int)
    #undistort the image
    #img2 = cv.undistort(img2, mtx, dist, None, newcameramtx)
    #Resize so it fits on the screen
    img2 = cv.resize(img2, (0, 0), fx = scale, fy = scale)
    cv.fillPoly(img2, [c2_pts], 255)
    cv.imshow(str(joints), img2)
    cv.waitKey(1)


cv.waitKey(0)
cv.destroyAllWindows()
