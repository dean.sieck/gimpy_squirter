#!/usr/bin/python
import time
import RPi.GPIO as GPIO
from functions.PanTilt_Driver import PCA9685
import functions.pt_kinematics as kine

pt=PCA9685()
try:
    pt.setPWMFreq(50)
    #pt.setServoPulse(1,500)
    a=pt.setRotationAngle(0, 0)
    b=pt.setRotationAngle(1, 90.5)
    print(a,b)
    input()
    
    a,b= pt.setPosition([0,120])
    print(a,b)


except:
    pt.exit_PCA9685()
    print ("\nError: Program end")
    exit()