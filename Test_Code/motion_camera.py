import RPi.GPIO as GPIO                       #Import GPIO library
from picamera2 import Picamera2, Preview
import time
import cv2
import numpy as np

#initialize camera
camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size": (640, 480)}, display="lores")
camera.configure(camera_config)
#camera.start_preview(Preview.QTGL)
camera.start()

#initialize motion sensor
# GPIO.setmode(GPIO.BOARD)                      #Set GPIO pin numbering
# pir = 8                                      #Associate pin 26 to pir
# GPIO.setup(pir, GPIO.IN)                      #Set pin as GPIO in 

time.sleep(2) #wait for initializations to finish
#get initial image
last_image = camera.capture_array()
last_image = cv2.cvtColor(last_image, cv2.COLOR_BGR2GRAY)
start=time.time()


pic = 0
motion = False
kernel = np.ones((5,5),np.uint8)

while True:

    #if GPIO.input(pir):             #Check whether pir is HIGH
        motion=True
        #print('motion detected')
        
        while motion:
            #start image capture
            image = camera.capture_array()          
            color_image = cv2.cvtColor( image, cv2.COLOR_BGR2RGB )
            image = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
            #check if motion is seen in camera (image subtraction)
            sub_mask = cv2.subtract(image,last_image)
            ret,bw_mask = cv2.threshold(sub_mask,10,255,cv2.THRESH_BINARY)
            open_mask = cv2.morphologyEx(bw_mask, cv2.MORPH_OPEN, kernel)
            diff = sum(sum(open_mask))
                        
            #if diff>100000:	#Image is different from last image
            if True:
                pic += 1
                print('saving image',pic, time.time()-start)
                cv2.imwrite(('/home/pi/Pictures/'+str(pic)+'.jpg'),color_image)
                last_image=image
                start=time.time()
            else:			#Image is same as last one
                motion=False


            time.sleep(0.5)  