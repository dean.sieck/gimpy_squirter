#!/usr/bin/python
import time
import RPi.GPIO as GPIO
from functions.Pan_Tilt_Control import pan_tilt
from picamera2 import Picamera2, Preview

pt=pan_tilt()

picam2 = Picamera2()
camera_config = picam2.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
picam2.configure(camera_config)
#picam2.start_preview(Preview.QTGL)
picam2.start()

path='/home/pi/Gimpy/test_pics/'

while True:
    p = int(input ('pan: '))
    t = int(input ('tilt: '))
    p,t= pt.setPosition([p,t])
    
    pic=input ('Picture?: ')
    if pic=='y':
        picam2.capture_file(path+str(p)+'_'+str(t)+'.jpg')
    elif pic=='s':
        break
    
    #re.findall('\d+', s) #gets all ints in a string (returned as string)
    


