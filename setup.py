import time
import torch
import cv2 as cv
import random
import numpy as np
import RPi.GPIO as GPIO
from picamera2 import Picamera2
from functions.Pan_Tilt_Control import pan_tilt
from functions.no_go_zones import Zones
from functions.yolo_results import yolo_model
from functions.camera_calibration import calibrate

def takepic(display=True,scale=0.8):
    global image_size, camera
    img=camera.capture_array()
    if display:
        small_img = cv.resize(img, (0, 0), fx = scale, fy = scale)
        small_img = cv.cvtColor(small_img, cv.COLOR_BGR2RGB)
        cv.imshow('Image', small_img)
        cv.waitKey(1)
    img = cv.resize(img, (0, 0), fx = image_size, fy = image_size)
    return img

neutral=np.loadtxt('/home/pi/Gimpy/Constants/neutral.csv')

pt=pan_tilt(neutral)
zones=Zones(pt)

camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
camera.configure(camera_config)
camera.start()
time.sleep(1)gi
LED_PIN = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.output(LED_PIN, GPIO.LOW)

x=input('Do you want to calibrate the camera? (y/n) ')
if x=='y':
    calibrate(camera)
    

x=input('Do you want to create new zones? (y/n) ')
if x=='y':
    takepic() #for some reason the first image is always black
    zones.make_zones(camera)

x=input('Do you want to change the neutral location? (y/n) ')
if x=='y':
    takepic()
    GPIO.output(LED_PIN, GPIO.HIGH)
    neutral=pt.click_control(camera)
    np.savetxt('/home/pi/Gimpy/Constants/neutral.csv',neutral)
    GPIO.output(LED_PIN, GPIO.LOW)