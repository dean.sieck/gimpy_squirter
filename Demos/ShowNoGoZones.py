import sys
sys.path.append("/home/pi/Gimpy")
#!/usr/bin/python
import time
import torch
import cv2 as cv
import random
import numpy as np
import RPi.GPIO as GPIO
from picamera2 import Picamera2
from functions.Pan_Tilt_Control import pan_tilt
from functions.no_go_zones import Zones
from functions.yolo_results import yolo_model


def showpic(img,scale=0.8):
    small_img = cv.resize(img, (0, 0), fx = scale, fy = scale)
    small_img = cv.cvtColor(small_img, cv.COLOR_BGR2RGB)
    cv.imshow('Image', small_img)
    cv.waitKey(1)
    
def mousePoints(event,x,y,flags,params):
    global point, clicked
    # Left button mouse click event 
    if event == cv.EVENT_LBUTTONDOWN:
        point = x,y
        clicked = True
    if event == cv.EVENT_MBUTTONDOWN:
        point = False
        clicked = True
    
    
####################################################
#################     SETUP        #################
####################################################

display=False
neutral=np.loadtxt('/home/pi/Gimpy/Constants/neutral.csv')
point = [1920/2,1080/2]
clicked = False

pt=pan_tilt(neutral)
zones=Zones(pt)
#yolo = yolo_model()

camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
camera.configure(camera_config)
camera.start()
time.sleep(1)

LED_PIN = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.output(LED_PIN, GPIO.LOW)

####################################################
#############     RUN TIME CODE        #############
####################################################
x=input('Do you want to create new zones? (y/n) ')
if x=='y':
    takepic() #for some reason the first image is always black
    zones.make_zones(camera)

print('left click to move camera')
print('middle click to stop')
while point:
    pt.approach_cord(point,reset=True)
    clicked = False
    start=time.time()
    #take a picture
    #time.sleep(1)
    img=camera.capture_array()
    dist=zones.dist_to_zone(img)
    showpic(img)
    
    while clicked == False:
        # Mouse click event on image
        cv.setMouseCallback("Image", mousePoints)
        # Refreshing window all time
        cv.waitKey(1)

        
cv.destroyAllWindows()
