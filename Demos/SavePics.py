import sys
sys.path.append("/home/pi/Gimpy")
#!/usr/bin/python
import time
import os
import glob
import torch
import cv2 as cv
import random
import numpy as np
import RPi.GPIO as GPIO
from picamera2 import Picamera2
from functions.Pan_Tilt_Control import pan_tilt
from functions.no_go_zones import Zones
from functions.yolo_results import yolo_model

def showpic(img, scale=0.8):
    small_img = cv.resize(img, (0, 0), fx = scale, fy = scale)
    small_img = cv.cvtColor(small_img, cv.COLOR_BGR2RGB)
    small_img = cv.rotate(small_img,cv.ROTATE_180)    
    cv.imshow('Image', small_img)
    cv.waitKey(1)
    
def savepic(folder,img):
    global path
    name=path+folder+str(round(10*(time.time()-gtime)))+'.jpg'
    image=cv.cvtColor(img, cv.COLOR_BGR2RGB)
    image = cv.rotate(image,cv.ROTATE_180)
    cv.imwrite(name,image)

    
    
def pick_corner():
    global neutral, pt
    #find potential distance to travel
    p_l=1-max(0,(neutral[0]-pt.angles[0])/neutral[0])
    p_r=1-max(0,(pt.angles[0]-neutral[0])/(180-neutral[0]))
    p_b=1-max(0,(neutral[1]-pt.angles[1])/neutral[1])
    p_t=1-max(0,(pt.angles[1]-neutral[1])/(180-neutral[1]))
    #calculate probabilities of each corner
    p_tr=p_t*p_r
    p_tl=p_t*p_l
    p_br=p_b*p_r
    p_bl=p_b*p_l
    
    #randomly select from  
    rand=random.random()
    prob=p_tr/(p_tr+p_tl+p_br+p_bl)
    if rand<prob: return 'tr'
    prob=p_tl/(p_tr+p_tl+p_br+p_bl) + prob
    if rand<prob: return 'tl'
    prob=p_br/(p_tr+p_tl+p_br+p_bl) + prob
    if rand<prob: return 'br'
    else: return 'bl'
    
    
def wiggle():
    global pt, camera
    start=time.time()
    time.sleep(0.5)
    img=camera.capture_array()
    while (time.time()-start)<5:
        r=[random.random()*5,random.random()*5]
        pt.setPosition(neutral)
        
def check_laser(double_check=False):
    global yolo, zones, camera, img
    #check if laser or cat are overlapping human
    #also check if laser is in no-go zone
    if yolo.not_human() and zones.dist_to_zone(img)>0:
        if double_check:
        #laser was safe to shine, quickly verify that it still is
            img=takepic(False)
            if yolo.rapid_check(img): 
                print('turn on laser')
                return True
            else:
                print('turn off laser')
                return False
        print('turn on laser')
        return True
    else:
        print('turn off laser')
        return False
        
    
    
####################################################
#################     SETUP        #################
####################################################

display=True
neutral=np.loadtxt('/home/pi/Gimpy/Constants/neutral.csv')
path='/home/pi/Gimpy/Demos/Pics/'

pt=pan_tilt(neutral)
zones=Zones(pt)
yolo = yolo_model()

camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
camera.configure(camera_config)
camera.start()
time.sleep(1)

LED_PIN = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)

input('Previously saved pics will be deleted')
for f in glob.glob(path+'cat_in_nogo/*.jpg'):
        os.remove(f)
for f in glob.glob(path+'laser_off/*.jpg'):
    os.remove(f)
for f in glob.glob(path+'laser_on/*.jpg'):
    os.remove(f)
for f in glob.glob(path+'no_cat/*.jpg'):
    os.remove(f)
for f in glob.glob(path+'wiggle_check/*.jpg'):
    os.remove(f)
    
####################################################
#############     RUN TIME CODE        #############
####################################################

gtime=time.time()
while True:
    start=time.time()
    #take a picture
    #time.sleep(1)
    img=camera.capture_array()
    # run yolo model on image
    cat_loc=yolo.find_cat(img)
    
    if cat_loc:
        print('cat found')
        #yolo.results.show()
        # check if the cat is in no-go zone
        dist=zones.dist_to_zone(img,cat_loc)
        img=np.array(yolo.results.render())[0]
        if display:
            showpic(img)
        if dist<0:#cat is in zone
            print('cat in no-go zone')
            savepic('cat_in_nogo/',img)
            #lure cat away from zone by returning to neutral
            print('wiggle')
            GPIO.output(LED_PIN, GPIO.LOW)
            pt.setPosition(neutral)
            time.sleep(0.3)
            img=camera.capture_array()
            yolo.find_cat(img)
            img=np.array(yolo.results.render())[0]
            savepic('wiggle_check/',img)
            if check_laser(): 
                print('turn on laser')
                GPIO.output(LED_PIN, GPIO.HIGH)
                wiggle()
            else:
                print('human in wiggle zone')
                      
            #wiggle()
        else:
            print('approaching cat')
            # center on the cat
            finished, centered = pt.approach_cord(cat_loc)
            print('checking if safe to shine laser')
            if check_laser(): 
                print('turn on laser')
                savepic('laser_on/',img)
                GPIO.output(LED_PIN, GPIO.HIGH)
            else:
                print('turn off laser')
                savepic('laser_off/',img)
                GPIO.output(LED_PIN, GPIO.LOW)
               
        #yolo.results.show()
        
            
    else:
        print('cat not found')
        GPIO.output(LED_PIN, GPIO.LOW)
        if display:
            zones.dist_to_zone(img)
            showpic(img)
        savepic('no_cat/',img)

        if (time.time()-yolo.cat_time)>5:
            print('returning to neutral')
            pt.setPosition(neutral)
    print('processing time: ', time.time()-start)
    

        
cv.destroyAllWindows()
