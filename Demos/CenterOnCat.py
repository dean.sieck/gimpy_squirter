import sys
sys.path.append("/home/pi/Gimpy")
#!/usr/bin/python
import time
import torch
import cv2 as cv
import random
import numpy as np
import RPi.GPIO as GPIO
from picamera2 import Picamera2
from functions.Pan_Tilt_Control import pan_tilt
from functions.no_go_zones import Zones
from functions.yolo_results import yolo_model

def takepic(display=True,scale=0.8):
    global image_size, camera
    img=camera.capture_array()
    if display:
        small_img = cv.resize(img, (0, 0), fx = scale, fy = scale)
        small_img = cv.cvtColor(small_img, cv.COLOR_BGR2RGB)
        cv.imshow('Image', small_img)
        cv.waitKey(1)
    return img

  
    
####################################################
#################     SETUP        #################
####################################################

display=True
neutral=np.loadtxt('/home/pi/Gimpy/Constants/neutral.csv')

pt=pan_tilt(neutral)
zones=Zones(pt)
yolo = yolo_model()

camera = Picamera2()
camera_config = camera.create_still_configuration(main={"size": (1920, 1080)}, lores={"size":  (1920, 1080)}, display="lores")
camera.configure(camera_config)
camera.start()
time.sleep(1)


LED_PIN = 21
GPIO.setmode(GPIO.BCM)
GPIO.setup(LED_PIN, GPIO.OUT)
GPIO.output(LED_PIN, GPIO.LOW)

####################################################
#############     RUN TIME CODE        #############
####################################################


while True:
    start=time.time()
    loop=time.time()
    #take a picture
    #time.sleep(1)
    img=takepic(display)
    print('pic:      ', time.time()-loop)
    # run yolo model on image
    loop=time.time()
    cat_loc=yolo.find_cat(img)
    print('yolo:     ', time.time()-loop)
    
    if cat_loc:
        print('cat found')
        loop=time.time()
        finished, centered = pt.approach_cord(cat_loc)
        if centered: 
            GPIO.output(LED_PIN, GPIO.HIGH)
            print('centered')
        else:
            GPIO.output(LED_PIN, GPIO.LOW)        
        print('approach: ', time.time()-loop)
    else:
        GPIO.output(LED_PIN, GPIO.LOW) 
    print('TOTAL:  ', time.time()-start)
    print('--------------------')
    

        
cv.destroyAllWindows()
