import time
import torch
import numpy as np
import cv2 as cv

class yolo_model:
    def __init__(self, model='n'):
        if model=='m': #medium
            self.model = torch.hub.load('ultralytics/yolov5', 'custom', path='/home/pi/Gimpy/yolov5/runs/train/Combined_medium_cat_coco_m/weights/best.pt', force_reload=True) 
        if model=='s': #small
            self.model = torch.hub.load('ultralytics/yolov5', 'custom', path='/home/pi/Gimpy/yolov5/runs/train/Combined_s/weights/best.pt', force_reload=True) 
        if model=='n': #nano
            self.model = torch.hub.load('ultralytics/yolov5', 'custom', path='/home/pi/Gimpy/yolov5/runs/train/Combined_n/weights/best.pt', force_reload=True)         
        self.cat_time=0 #time cat was last detected
        self.time_threshold=3
        
    def find_cat(self,img,loc='c'):
        # get results from yolo model
        self.results = self.model(img)
        r=self.results.pandas().xyxy[0]
        self.cats = r.loc[r['name']=='cat']
        self.people = r.loc[r['name']=='person']
        if self.cats.empty:
            return False
        else:
            #choose the best cat and return its location
            self.pick_best_cat()
            cat_loc=[((self.cats.xmax[self.best_cat]+self.cats.xmin[self.best_cat])/2), ((self.cats.ymax[self.best_cat]+self.cats.ymin[self.best_cat])/2)]
            #pick location based on requested corner
            if loc=='c': return cat_loc
            if loc=='tl': return [self.cats.xmin[self.best_cat],self.cats.ymin[self.best_cat]]
            if loc=='tr': return [self.cats.xmax[self.best_cat],self.cats.ymin[self.best_cat]]
            if loc=='bl': return [self.cats.xmin[self.best_cat],self.cats.ymax[self.best_cat]]
            if loc=='br': return [self.cats.xmax[self.best_cat],self.cats.ymax[self.best_cat]]
    
    def not_human(self,extra_safe=False):
        # check if that cat is overlapping with a person
        if self.people.empty:
            return True
        elif extra_safe==True:
            #dont shoot the laser if any humans are present
            return False
        #edge case for wiggle function
        if self.cats.empty:
            return True
        # if none of these are true then there is no intersection
        x1=(self.cats.xmin[self.best_cat]>self.people.xmax)
        y1=(self.cats.ymin[self.best_cat]>self.people.ymax)
        x2=(self.cats.xmax[self.best_cat]<self.people.xmin)
        y2=(self.cats.ymax[self.best_cat]<self.people.ymin)
        cat = (x1+x2+y1+y2).any()
        
        # check if the camera center is overlapping with a person
        center=[1920/2,1080/2]
        # if none of these are true then there is no intersection
        x1=(center[0]>self.people.xmax)
        y1=(center[1]>self.people.ymax)
        x2=(center[0]<self.people.xmin)
        y2=(center[1]<self.people.ymin)
        cam = (x1+x2+y1+y2).any()
        
        return (cat and cam)
    
    def pick_best_cat(self):
        # a cat has been found, record the time
        self.cat_time=time.time()
        #cat with highest confidence is the most interesting one
        self.best_cat=self.cats.loc[self.cats['confidence']==max(self.cats['confidence'])].index[0]
        # alternative would be to look for the cat closest to a no-go zone

    def rapid_check(self,orig_img):
        #to double check that the cat is still there before shooting
        center=[1920/2,1080/2]
        n=2
        #shrink the search region of the image to n*cat size
        img=orig_img.copy()
        dx = self.cats.xmax[self.best_cat]-self.cats.xmin[self.best_cat]
        dy = self.cats.ymax[self.best_cat]-self.cats.ymin[self.best_cat]
        xmin = int(min(0, center[0] - n*dx))
        xmax = int(max(1920, center[0] + n*dx))           
        ymin = int(min(0, center[1] - n*dy))
        ymax = int(max(1080, center[1] + n*dy))
        
        img=img[xmin:xmax,ymin:ymax]
        self.results = self.model(img)
        
        #returns true if no humans are detected
        if self.people.empty:
            return True
        else:
            return False
        
            