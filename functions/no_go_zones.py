import cv2 as cv
import numpy as np
import time
import glob
import os



class Zones:
    def __init__(self,pan_tilt):
        #initialize pantilt
        self.pt=pan_tilt
        # load Camera Intrinsic parameters
        self.mtx = np.loadtxt('/home/pi/Gimpy/Constants/Camera/mtx.csv', delimiter=',')
        self.dist = np.loadtxt('/home/pi/Gimpy/Constants/Camera/dist.csv', delimiter=',')
        # load no-go zones
        self.zones=[]
        self.scan_loc = np.loadtxt('/home/pi/Gimpy/Constants/NoGo_Zones/scan.csv')
        for f in glob.glob('/home/pi/Gimpy/Constants/NoGo_Zones/zone_*.csv'):
                self.zones.append(np.matrix(np.loadtxt(f)))
        self.scan_count=0
    
    def scan(self):
        if self.scan_count>=self.scan_loc.shape[0]:
            self.scan_count=0
        self.pt.setPosition(self.scan_loc[self.scan_count])
        self.scan_count+=1
        
    def make_zones(self, camera,scale=0.8):
        num=0
        #delete current zones
        self.zones=[]
        for f in glob.glob('/home/pi/Gimpy/Constants/NoGo_Zones/zone_*.csv'):
                os.remove(f)
        scan_loc=[]
        #get new zones
        finished=False
        while finished==False:
            # move camera to desired location
            pic='n'
            while pic != 'y':
                p = int(input ('pan: '))
                t = int(input ('tilt: '))
                angles= self.pt.setPosition([p,t])
                img=camera.capture_array()
                img=cv.cvtColor(img, cv.COLOR_BGR2RGB)
                img = cv.resize(img, (0, 0), fx = scale, fy = scale)
                cv.imshow("Image", img)
                cv.waitKey(1)
                pic=input ('Draw Zone?: ')
            # draw rectangle around zone
            input_pts = self.draw_rect(img).astype('float32')
            input_pts = input_pts*1/scale
            
            x = input('save zone? (y/n) ')
            if x == 'y':
                # convert to world coordinates
                world_cord=self.local_to_world(input_pts, angles)
                self.zones.append(world_cord)
                scan_loc.append([angles[0],angles[1]]) #otherwise it appends a pointer to angles
                # save coordinates to file
                np.savetxt('/home/pi/Gimpy/Constants/NoGo_Zones/zone_'+str(num)+'.csv',world_cord)
                num+=1
                
            x = input('Create new zone? (y/n) ')
            if x == 'y':
                finished = False
            else:
                finished = True
        self.scan_loc=np.asarray(scan_loc)
        np.savetxt('/home/pi/Gimpy/Constants/NoGo_Zones/scan.csv',scan_loc)

    def mousePoints(self,event,x,y,flags,params):
        # Left button mouse click event 
        if event == cv.EVENT_LBUTTONDOWN:
            self.point_matrix= np.append(self.point_matrix,np.zeros((1,2),int),axis=0)
            self.point_matrix[self.counter] = x,y
            self.counter = self.counter + 1
            self.clicked = True

    def draw_rect(self, orig_img):
        img = orig_img.copy()
        self.point_matrix = np.zeros((0,2),int)
        self.counter = 0
        self.clicked = False
        print('left click to add points')

        while True:
            
            # Showing image
            cv.imshow("Image", img)
            cv.waitKey(1)
            while self.clicked == False:
                # Mouse click event on image
                cv.setMouseCallback("Image", self.mousePoints)
                # Refreshing window all time
                cv.waitKey(1)
            #print(self.point_matrix)
            self.clicked = False
                   
            if self.counter>1:
                #show the zone you are drawing      
                cv.fillPoly(img, [self.point_matrix], (100,255,255))

            if self.counter>=4:
                cv.imshow("Image", img)
                cv.waitKey(1)
                return self.point_matrix
            
    def local_to_world(self, input_pts, joints):
        side1 = float(input ('length of side 1 (cm): '))*10
        side2 = float(input ('length of side 2 (cm): '))*10
        zone_pts = np.float32([[0, 0, 0],
                                [0, side1,0],
                                [side2, side1,0],
                                [side2, 0,0]])

        #get camera frame relative to zone frame
        _,rot,P =cv.solvePnP(zone_pts,input_pts,self.mtx,self.dist)
        #convert into translation matrix
        c1_T_z=np.matrix(np.identity(4))
        R,_=cv.Rodrigues(rot)
        c1_T_z[0:3,0:3]=R
        c1_T_z[0:3,3]=P
        z_T_c1=np.linalg.inv(c1_T_z)
        w_T_c1=np.matrix(np.identity(4))

        # # get transformation matrix from camera 1 to world (world = camera1 for this test)
        w_T_c1=self.pt.transform_matrix(joints)
        c1_T_w=np.linalg.inv(w_T_c1)

        world_cord=np.matrix(np.zeros([4,4]))
        for i in range (0,4):
            #save world coordinates
            z= np.matrix([zone_pts[i,0],zone_pts[i,1],0,1]).transpose()
            world_cord[:,i]=w_T_c1*c1_T_z*z
        return world_cord
    
    def dist_to_zone(self, img, point=[1920/2,1080/2], display=False, scale=0.8):
        #calculate transformation from camera2 to world
        w_T_c2=self.pt.transform_matrix()
        c2_T_w=np.linalg.inv(w_T_c2)
        
        #for each zone
        dist=[]
        for world_cord in self.zones:
                #get zone coordinates from world frame to camera frame
                output_pts=np.zeros([4,2])
                cam2_cord=np.matrix(np.zeros([4,4]))
                I_=np.matrix(np.array([[1,0,0,0],[0,1,0,0],[0,0,1,0]]))
                for i in range (0,4):
                        cam2_cord[:,i]=c2_T_w*world_cord[:,i]
                        pic2_cord=self.mtx*I_*cam2_cord[:,i]
                        pic2_cord=pic2_cord*(1/pic2_cord[2])
                        output_pts[i,:]=pic2_cord[:2].transpose()
                        
                #add to list of zone coordinates for picture
                output_pts=output_pts.astype(int)
                #draw points on image
                c2_pts=output_pts
                #cv.fillPoly(img, [c2_pts], (100,255,255))
                cv.polylines(img, [c2_pts], True,(100,255,255),10)

                #get distance to zone
                dist.append(-cv.pointPolygonTest(output_pts, point, True))
        if display:
                #Resize so image fits on the screen
                img = cv.resize(img, (0, 0), fx = scale, fy = scale)
                cv.imshow('Image', img)
                cv.waitKey(1)
                
        return min(dist)
    
 
        
