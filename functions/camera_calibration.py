import numpy as np
import cv2 as cv
import glob
import os

size=298.5 # size of entire grid (n+1 squares)
n=7 # number of internal corners (one dimension)
path= '/home/pi/Gimpy/Constants/Camera/'
#path= '/home/dean/pi/Gimpy/Constants/Camera/'


def calibrate(camera,save=True):
    global size, n, path
    # termination criteria
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((n*n,3), np.float32)
    objp[:,:2] = (np.mgrid[0:n,0:n]*size/(n+1)).T.reshape(-1,2)
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    if save:
        for f in glob.glob(path+'Pictures/*.jpg'):
            os.remove(f)
    images=0
    print('Take photos')
    while images<20:
        img = camera.capture_array()
        cv.imshow('img', img)
        cv.waitKey(1)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, (n,n), None)        # If found, add object points, image points (after refining them)
        if ret == True:
            print('image taken: ',images)
            if save:
                cv.imwrite(path+'Pictures/'+str(images)+'.jpg', img)
            objpoints.append(objp)
            corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)
            images+=1
            # Draw and display the corners
            cv.drawChessboardCorners(img, (n,n), corners2, ret)
            cv.imshow('img', img)
            cv.waitKey(500)
    cv.destroyAllWindows()
    
    print('performing calibration')
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    if save:
        np.savetxt(path+'mtx.csv', mtx)
        np.savetxt(path+'dist.csv', dist)
        print('Camera Calibration Data Saved')
    
    #calculate reprojection error
    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
        mean_error += error
    print( "total error: {}".format(mean_error/len(objpoints)) )


def calibrate_saved_images():
    global size, n, path
    # termination criteria
    criteria = (cv.TERM_CRITERIA_EPS + cv.TERM_CRITERIA_MAX_ITER, 30, 0.001)
    # prepare object points, like (0,0,0), (1,0,0), (2,0,0) ....,(6,5,0)
    objp = np.zeros((n*n,3), np.float32)
    objp[:,:2] = (np.mgrid[0:n,0:n]*size/n).T.reshape(-1,2)
    # Arrays to store object points and image points from all the images.
    objpoints = [] # 3d point in real world space
    imgpoints = [] # 2d points in image plane.
    images = glob.glob(path+'Pictures/*.jpg')
    for fname in images:
        img = cv.imread(fname)
        gray = cv.cvtColor(img, cv.COLOR_BGR2GRAY)
        # Find the chess board corners
        ret, corners = cv.findChessboardCorners(gray, (n,n), None)
        # If found, add object points, image points (after refining them)
        if ret == True:
            objpoints.append(objp)
            corners2 = cv.cornerSubPix(gray,corners, (11,11), (-1,-1), criteria)
            imgpoints.append(corners2)
            # Draw and display the corners
            #cv.drawChessboardCorners(img, (n,n), corners2, ret)
            #cv.imshow('img', img)
            #cv.waitKey(500)
    cv.destroyAllWindows()
    
    print('performing calibration')
    ret, mtx, dist, rvecs, tvecs = cv.calibrateCamera(objpoints, imgpoints, gray.shape[::-1], None, None)

    np.savetxt(path+'mtx.csv', mtx)
    np.savetxt(path+'dist.csv', dist)
    print('Camera Calibration Data Saved')
    
    #calculate reprojection error
    mean_error = 0
    for i in range(len(objpoints)):
        imgpoints2, _ = cv.projectPoints(objpoints[i], rvecs[i], tvecs[i], mtx, dist)
        error = cv.norm(imgpoints[i], imgpoints2, cv.NORM_L2)/len(imgpoints2)
        mean_error += error
    print( "total error: {}".format(mean_error/len(objpoints)) )
