import numpy as np
import time
import cv2 as cv
from functions.PanTilt_Driver import PCA9685


class pan_tilt:
    def __init__(self, angles=[90,90]):
        self.angles=angles
        #initialize Pan Tilt Driver
        self.pt=PCA9685()
        try:
            self.pt.setPWMFreq(50)
        except:
            self.pt.exit_PCA9685()
            print ("\nError: Program end")
            exit()
        self.angles=self.setPosition(angles)
        #init gloabl variables for PID approaching coordinates
        self.e_i=0
        self.e_last=0

    def setPosition(self,pt_angles,delay=0.5):
        self.angles[0]=self.pt.setRotationAngle(0,pt_angles[0])
        self.angles[1]=self.pt.setRotationAngle(1,pt_angles[1])
        time.sleep(delay)
        return self.angles 

    def incrementPosition(self,increments):
        delay=max(abs(increments))*1/7 #rotation speed of motor 60deg/0.1s
        self.setPosition(self.angles+increments,delay)
        return self.angles 

    def approach_cord(self,cord,reset=False, threshold=50, center=np.array([1920,1080])/2):
        if reset:
            self.e_i=0
            self.e_last=0
        direction=[-1,1]
        kp, ki, kd = [1,1,1]
        #dependent gains
        kc=5/min(center)
        kp, ki, kd = [kc,kc/2,kc*.7]
        kp, ki, kd = [kc,kc*0,kc*.7]
        
        e=center-cord
        self.e_i+=e
        u=(kp*e+ki*self.e_i+kd*(self.e_last-e))*direction

        self.e_last=e
        self.incrementPosition(u)
        #print('distance from center: ',e)
        
        centered=list(abs(num) < threshold  for num in e)
        limited=[(self.angles[0]==180 or self.angles[0]==0),
                 (self.angles[1]==170 or self.angles[1]==11)]
        finished=[centered[i] or limited[i] for i in range(2)]
                
        return all(finished), all(centered)
                       

    def transform_matrix(self,theta=[]):
        if not theta: theta=self.angles
        theta=np.array(theta)*np.pi/180
        theta=np.append(theta,-np.pi/2)
        alpha=[np.pi/2,-np.pi/2,0]
        #a=[27.625,-11.125,0]
        #d=[40,0,20] 
        a=[28.25,-14.675,0]
        d=[40,0,15.35] 
        
        A=[np.matrix(np.zeros([4,4]))]*3
        
        for i in range(0,3):
            A[i]=np.matrix([[np.cos(theta[i]),-np.sin(theta[i])*np.cos(alpha[i]),np.sin(theta[i])*np.sin(alpha[i]),a[i]*np.cos(theta[i])],
                            [np.sin(theta[i]),np.cos(theta[i])*np.cos(alpha[i]),-np.cos(theta[i])*np.sin(alpha[i]),a[i]*np.sin(theta[i])],
                            [0,np.sin(alpha[i]),np.cos(alpha[i]),d[i]],
                            [0,0,0,1]])
        
        T=A[0]*A[1]*A[2]
        return T
    
    
    
    def mousePoints(self,event,x,y,flags,params):
        # Left button mouse click event 
        if event == cv.EVENT_LBUTTONDOWN:
            self.point = x,y
            self.clicked = True
        if event == cv.EVENT_MBUTTONDOWN:
            self.point = False
            self.clicked = True
    
    def click_control(self,camera,scale=0.8):
        self.point = [1920/2,1080/2]
        
        print('left click to move camera')
        print('middle click to stop')
        
        while self.point:
            self.approach_cord(self.point,reset=True)
            self.clicked = False
            #take a picture
            img=camera.capture_array()
            img=cv.cvtColor(img, cv.COLOR_BGR2RGB)
            img = cv.resize(img, (0, 0), fx = scale, fy = scale)
            cv.imshow("Image", img)
            cv.waitKey(1)

            while self.clicked == False:
                # Mouse click event on image
                cv.setMouseCallback("Image", self.mousePoints)
                # Refreshing window all time
                cv.waitKey(1)
                
        return self.angles
            

            
            
            
        
        
        

     




